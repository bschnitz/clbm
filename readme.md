# Warning

this project is just a stub at the moment!

# Commandline-Line Bookmark Manager

## Contents

#### [1. Usage](#markdown-header-usage)
#### [2. Dependencies](#markdown-header-dependencies)
#### [3. Installation](#markdown-header-installation)
#### [4. ToDo](#markdown-header-todo)
#### [5. License](#markdown-header-license)

## Usage

the following assumes, that You installed clbm as explained in
[Installation](#markdown-header-installation).

```sh
# add a bookmark for the current directory
bm add myproject

# list all bookmarks
bm list

# cd to a bookmark
bm jump myproject

# tag the myproject bookmark with mytag
bm tag myproject mytag

# list all tags
bm tag

# list all bookmarks with the specified tags
bm list mytag,myothertag

# check for updates and install them
bm update

# show help text
bm help
```

You may want to define an alias for ``bm jump``:
```sh
alias gg="bm jump"
```
Now You can just use ``gg myproject`` to jump to the bookmark named 'myproject'.

## Dependencies

for running clbm

* python3
* bash or zsh (other shells may be compatible too)
* dirname

for installing clbm

* python3
* bash or zsh (other shells may be compatible too)
* git 
* wget or curl

## Installation

put the following lines into Your shell configuration file
(e.g. ~/.bashrc or ~/.zshrc).
```sh
# Command-Line Bookmark Manager (clbm) - Installation and Setup
clbm_namespace(){
  # download/install clbm (will only be executed, if clbm is not yet installed)
  local install_path="$HOME/.clbm" # where to install clbm (required!)
  local installer_url=https://bitbucket.org/bschnitz/clbm/raw/0.2.1/install
  [ -e "$install_path" ] || eval "$(wget -q $installer_url -O -)"

  # setup clbm and set the command name to 'bm' (defaults otherwise to 'clbm')
  source "$install_path/setup" bm
}
clbm_namespace # execute the code defined above
unset -f clbm_namespace # delete the clbm_namespace definition
```
the next time your shell is started, clbm will be automatically installed.

if You need to use curl instead of wget to download the installer, replace
``wget -q $clbm_installer_url -O -`` with ``curl $clbm_installer_url``.

## ToDo

- autocompletion for shells (bash, zsh)
- integration of fzf
- inverse lookup (path to bookmark name)

## License

[GPLv3](gpl-3.0.txt)
